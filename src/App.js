import "bootstrap/dist/css/bootstrap.min.css";
import Intro from "./Intro";
import Solution from "./Solution";
import "./styles.css";

export default function App() {
  return (
    <div className="App p-2">
      {/* <Intro /> */}
      <Solution />
    </div>
  );
}
