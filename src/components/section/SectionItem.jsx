
const SectionItem = (props) => {
  const showMoreDetailHandler = () => {
    if (!showItemDetail) {
      props.onSetActiveSection(props.section.title);
    } else {
      props.onSetActiveSection(null);
    }
  };

  var showItemDetail =
    props.activeSection == props.section.title ? true : false;

  return (
    <>
      {
        <div className="border border-top-0 rounded-2">
          <div
            className={
              (showItemDetail ? "alert alert-info " : "") +
              " text-primary py-2 px-3 d-flex justify-content-between m-0 border-top"
            }
            onClick={showMoreDetailHandler}
          >
            <p className="m-0">{props.section.title}</p>
            {showItemDetail ? (
              <i className="bi bi-chevron-up"></i>
            ) : (
              <i className="bi bi-chevron-down"></i>
            )}
          </div>
          {showItemDetail && <div className="py-2">{props.children}</div>}
        </div>
      }
    </>
  );
};

export default SectionItem;
