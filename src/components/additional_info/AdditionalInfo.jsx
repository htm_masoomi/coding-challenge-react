import { useEffect, useState } from "react";
import { Form, Button } from "react-bootstrap";
import styles from './AdditionalInfo.module.css'

const AdditionalInfo = (props) => {

  const [form, setForm] = useState(null);

  const inputChangedHandler = (event) => {
    setForm((prevStatus) => {
      return { ...prevStatus, [event.target.name]: event.target.value };
    });
  };

  useEffect(() => {
    props.data.map((item) => {
      setForm((prevStatus) => {
        return { ...prevStatus, [item.name]: item.value || '' };
      });
    });
  }, []);

  const handleData =(event) => {
    event.preventDefault()
    console.log(form);
  }

  return (
    <Form onSubmit={handleData} className="d-flex flex-wrap">
      {props.data.map((item) => (
        <Form.Group
          className="col-4 my-2 d-flex flex-column px-2"
          key={item.id}
        >
          <Form.Label className="d-flex justify-content-start mb-1">
            {item.label}
          </Form.Label>
          <Form.Control
            onChange={inputChangedHandler}
            name={item.name}
            as={item.type}
            defaultValue={item.value}
            placeholder="Enter Here"
          />
        </Form.Group>
      ))}

      <Button className={styles.submit_button} type="submit">Save</Button>
    </Form>
  );
};

export default AdditionalInfo;
